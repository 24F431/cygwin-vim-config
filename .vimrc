"        _                    
" __   _(_)_ __ ___  _ __ ___ 
" \ \ / / | '_ ` _ \| '__/ __|
"  \ V /| | | | | | | | | (__ 
"   \_/ |_|_| |_| |_|_|  \___|
                             
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

    Plugin 'VundleVim/Vundle.vim'
    Plugin 'itchyny/lightline.vim'

call vundle#end()

let mapleader=" "
set tabstop=4 softtabstop=4
set shiftwidth=4
set expandtab
set smartindent
set nu
set hidden
set ignorecase
set incsearch
set scrolloff=8
" set signcolumn=yes

filetype plugin indent on
set nu
set laststatus=2
"set noshowmode
"set colorcolumn=80
nnoremap <leader>c :execute "set colorcolumn="
    \ . (&colorcolumn == "" ? "80" : "")<CR>
highlight ColorColumn ctermbg=DarkMagenta
" V-BLOCK remap
nnoremap <leader>v <c-v>

" copy + paste clipboard
vnoremap <C-k> "+y
map <C-e> "+P
" map undo search highlight
map <leader>h :let @/ = ""<cr>
" split and filemanager
nnoremap <leader>pv :wincmd v<bar> :Ex <bar> :vertical resize 30<cr>
" yank to the end
nnoremap Y y$
" moving text
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv
inoremap <C-j> <esc>:m .+1<CR>==
inoremap <C-k> <esc>:m .-2<CR>==
nnoremap <leader>j :m .+1<CR>==
nnoremap <leader>k :m .-2<CR>==
" list to show ALL white space characters as a character
set listchars=eol:$,tab:>·,trail:~,extends:>,precedes:<,space:␣
