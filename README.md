
# vimrc for cygwin vim in Windows

this is my customized vimrc for the cygwin vim version on Microsoft Windows

## installation

copy the .vimrc file to the home directory

## preview of vim for cygwin

![cygwin preview](./cygwin vim preview image.png)
